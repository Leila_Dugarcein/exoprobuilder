﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemy : MonoBehaviour
{
    Transform player;
    //PlayerHealth playerHealth;      
    Ennemy enemyHealth;
    public int health;
    UnityEngine.AI.NavMeshAgent nav;
    public bool followPlayer;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent<Ennemy>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        health = 100;
        followPlayer = false;
    }


    void Update()
    {
        if (health > 0 && followPlayer == true)
        {
            nav.SetDestination(player.position);
        }
        else
        {
            nav.SetDestination(transform.position);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            health = health - 50;
        }

        if (health == 0)
        {
            Destroy(this.gameObject);
        }
    }

     public void OnTriggerStay(Collider collision){
        if (collision.gameObject.tag =="MainCamera"){
            followPlayer = true;
        }
    }

    public void OnTriggerExit(Collider exit){
        if (exit.gameObject.tag == "MainCamera"){
            followPlayer = false;
        }
    }
}
